from django import forms

class Friend_Form(forms.Form):
    error_messages = {
        'required': 'This part has to be filled',
        'invalid': 'Not a valid URL',
    }
    attrs = {
        'class': 'form-control'
    }

    name = forms.CharField(label='Name', required=True, max_length=35, widget=forms.TextInput(attrs=attrs), error_messages=error_messages)
    website = forms.CharField(label='Website', required=True, widget=forms.URLInput(attrs=attrs), error_messages=error_messages)
