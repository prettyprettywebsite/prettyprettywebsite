from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .models import AddFriend
from .forms import Friend_Form
from .views import index, friend_post

# Create your tests here.

class PrettyFriendUnitTest(TestCase):
    
    def test_add_friend_url_is_exist(self):
        response = Client().get('/add-friend/')
        self.assertEqual(response.status_code, 200)

    def test_add_friend_using_index_func(self):
        found = resolve('/add-friend/')
        self.assertEqual(found.func, index)

    def test_landing_page_is_completed(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')

    def test_model_can_create_new_friend(self):
        #Creating a new friend
        new_activity = AddFriend.objects.create(name='Pengabdi Basekem',website='http://pandapacil.herokuapp.com')

        #Retrieving all available activity
        counting_all_available_friends= AddFriend.objects.all().count()
        self.assertEqual(counting_all_available_friends,1)

    def test_form_validation_for_blank_items(self):
        form = Friend_Form(data={'name': '', 'website': ''})
        self.assertFalse(form.is_valid())

    def test_add_friend_post_success_and_render_the_result(self):
        name = 'Pelle K'
        url = 'http://app-name-here-lol.herokuapp.com'
        response = Client().post('/add-friend/add_friend', {'name': name, 'website': url})
        self.assertEqual(response.status_code, 302)
        response = Client().get('/add-friend/')
        html_response = response.content.decode('utf8')
        self.assertIn(name, html_response)
        self.assertIn(url, html_response)