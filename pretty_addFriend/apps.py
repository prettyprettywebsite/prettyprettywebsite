from django.apps import AppConfig


class PrettyAddfriendConfig(AppConfig):
    name = 'pretty_addFriend'
