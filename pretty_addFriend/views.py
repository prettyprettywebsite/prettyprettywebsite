from django.shortcuts import render
from .models import AddFriend
from django.http import HttpResponseRedirect
from .forms import Friend_Form

# Create your views here.
response = {}
html = 'add_friend.html'

def index(request):
    response['Friend_form'] = Friend_Form
    addfriend = AddFriend.objects.all()
    response['addfriend'] = addfriend
    return render(request, html , response)

def friend_post(request):
    form = Friend_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['website'] = request.POST['website']
        addfriend = AddFriend(name=response['name'], website=response['website'])
        addfriend.save()      
    return HttpResponseRedirect('/add-friend/')


