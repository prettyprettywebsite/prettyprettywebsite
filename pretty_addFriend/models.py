from django.db import models

# Create your models here.

class AddFriend(models.Model):
    name = models.TextField(max_length=35)
    website = models.URLField()
    created_date = models.DateTimeField(auto_now_add=True)