from django.test import TestCase, Client
from django.urls import resolve
from .views import index, name1
from django.http import HttpRequest
from pretty_addFriend.models import AddFriend
from pretty_update_status.models import Status
from pretty_update_status.forms import Status_Form


# need to test for friend, feed counter and name?

class StatsTest(TestCase):
    def test_stats_url_is_exist(self):
        response = Client().get('/stats/')
        self.assertEqual(response.status_code,200)

    def test_stats_using_base_template(self):
        response = Client().get('/stats/')
        self.assertTemplateUsed(response, 'base.html')

    def test_stats_using_index_func(self):
        found = resolve('/stats/')
        self.assertEqual(found.func, index)

    def test_frNo(self):
    	new_activity = AddFriend.objects.create(name='Pengabdi Basekem',website='http://pandapacil.herokuapp.com')
    	frNumber = AddFriend.objects.count()
    	self.assertTrue(frNumber, 1)

    def test_feedNo(self):
        new_status = Status.objects.create(message='trying to do ppw project 1')
        feedNo = Status.objects.count()
        self.assertTrue(feedNo, 1)

    def test_lastP(self):
        LP = Status.objects.create(message='trying to do ppw project 1')
        self.assertTrue(LP, 'trying to do ppw project 1')
