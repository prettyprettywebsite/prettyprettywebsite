from django.shortcuts import render, redirect
from datetime import datetime, date
from pretty_addFriend.models import AddFriend
from pretty_update_status.models import Status

#from some other views.py import name, friendCounter, feedCounter
# Create your views here.


# Enter your name here
name1 = 'Team PrettyPrettyWebsite' # variable to import maybe?

# Create your views here.
def index(request):
	frNumber = AddFriend.objects.count()
	feedNo = Status.objects.count()
	lastPost = Status.objects.last()
	response = {'name': name1, 'frNo': frNumber, 'feed': feedNo, 'LPost': lastPost}

	return render(request, 'content.html', response)
