from django.conf.urls import url
from .views import index, status_update

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_status', status_update, name='add_status'),
]
