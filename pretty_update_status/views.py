from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status
# Create your views here.
response = {}
def index(request):
    response['author'] = "Team PrettyPrettyWebsite"
    status = Status.objects.all()
    response['status'] = status
    html = 'pretty_update_status.html'
    response['status_form'] = Status_Form
    return render(request, html, response)

def status_update(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['message'] = request.POST['message']
        status = Status(message=response['message'])
        status.save()
        return HttpResponseRedirect('/update-status/')
    else:
        return HttpResponseRedirect('/update-status/')
