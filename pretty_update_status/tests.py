from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, status_update
from .models import Status
from .forms import Status_Form
import unittest

# Create your tests here.
class PrettyUpdateStatusUnitTest(TestCase):

    def test_update_status_url_is_exist(self):
        response = Client().get('/update-status/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/update-status/')
        self.assertEqual(found.func, index)

    def test_stats_using_base_template(self):
        response = Client().get('/update-status/')
        self.assertTemplateUsed(response, 'base.html')

    def test_model_can_create_new_status(self):
        # Creating a new activity
        new_activity = Status.objects.create(message='trying to do ppw project 1')

        # Retrieving all available activity
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'message': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['message'],
            ["This field is required."]
        )

    def test_prettyupdatestatus_post_success_and_render_the_result(self):
        test = 'empty'
        response_post = Client().post('/update-status/add_status', {'message': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/update-status/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_prettyupdatestatus_post_error_and_render_the_result(self):
        test = 'empty'
        response_post = Client().post('/update-status/add_status', {'message': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/update-status/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)
